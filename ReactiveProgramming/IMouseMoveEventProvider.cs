﻿using System.Windows.Input;

namespace ReactiveProgramming
{
	public interface IMouseMoveEventProvider
	{
		event MouseEventHandler MouseMove;
	}
}