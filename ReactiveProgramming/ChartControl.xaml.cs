﻿using System.Windows.Controls;

namespace ReactiveProgramming
{
	/// <summary>
	/// Interaction logic for ChartControl.xaml
	/// </summary>
	public partial class ChartControl : UserControl
	{
		public ChartControl()
		{
			InitializeComponent();
		}
	}
}
