using System.Windows;
using System.Windows.Input;

namespace ReactiveProgramming
{
	class MousePositionProvider : IMousePositionProvider
	{
		private readonly IInputElement _inputElement;

		public MousePositionProvider(IInputElement inputElement)
		{
			_inputElement = inputElement;
		}

		public Point GetPosition(MouseEventArgs mouseEventArgs)
		{
			return mouseEventArgs.GetPosition(_inputElement);
		}
	}
}