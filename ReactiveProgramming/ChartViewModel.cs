﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace ReactiveProgramming
{
	public class ChartViewModel
	{
		private readonly Dispatcher _dispatcher = Application.Current.Dispatcher;

		private readonly IMousePositionProvider _mousePositionProvider;
		private readonly DataModel _horizontalDistance = new DataModel(0, 0);
		private readonly DataModel _verticalDistance = new DataModel(1, 0);
		private IDisposable _averageSpeedSubscription;

		public ObservableCollection<DataModel> LineCollection { get; private set; }
		public ObservableCollection<DataModel> StackedCollection { get; private set; }
		
		public ChartViewModel(
			IMousePositionProvider mousePositionProvider,
			IObservable<EventPattern<MouseEventArgs>> mouseMoveInputObservable)
		{
			_mousePositionProvider = mousePositionProvider;
			LineCollection = new ObservableCollection<DataModel>();
			StackedCollection = new ObservableCollection<DataModel>
			{
				_horizontalDistance,
				_verticalDistance
			};

			SubscribeToMouseEventObservable(mouseMoveInputObservable);
		}

		public void Dispose()
		{
			_averageSpeedSubscription.Dispose();
		}

		private void SubscribeToMouseEventObservable(IObservable<EventPattern<MouseEventArgs>> mouseMoveInputObservable)
		{
			var mouseMoveObservable = mouseMoveInputObservable.Select(CreateMouseEventInfo);
			var bufferedMouseObservable = mouseMoveObservable.Buffer(2);
			var mouseSpeedObservable = bufferedMouseObservable.Select(CreateMouseSpeedInfoFromMouseEvents).Where(x => x != null);

			mouseSpeedObservable.Subscribe(AddDistanceToStackedCollection);

			var bufferedSpeedObservable = mouseSpeedObservable.Buffer(TimeSpan.FromMilliseconds(500));
			var averagedSpeedObservable = bufferedSpeedObservable.Select(x =>
			{
				if (x.Any())
				{
					return x.Select(y => y.Speed).Average();
				}

				return 0;
			});

			_averageSpeedSubscription = averagedSpeedObservable.Subscribe(AddNewPointToChartCollection);
		}

		private void AddDistanceToStackedCollection(MouseSpeedInfo info)
		{
			_horizontalDistance.Y += Math.Abs(info.XDistance);
			_verticalDistance.Y += Math.Abs(info.YDistance);

			StackedCollection.Clear();
			StackedCollection.Add(_horizontalDistance);
			StackedCollection.Add(_verticalDistance);
		}

		private MouseSpeedInfo CreateMouseSpeedInfoFromMouseEvents(IList<MouseEventInfo> mouseEventArgsList)
		{
			Debug.Print("Mouse event args[0]: {0}", GetDebugInfoForMouseEventArgs(mouseEventArgsList[0]));
			Debug.Print("Mouse event args[1]: {0}", GetDebugInfoForMouseEventArgs(mouseEventArgsList[1]));

			int timeDifference = mouseEventArgsList[1].Timestamp - mouseEventArgsList[0].Timestamp;
			if (timeDifference > 0)
			{
				double distance = CalculateDistance(mouseEventArgsList[0].Point, mouseEventArgsList[1].Point);
				return new MouseSpeedInfo
				{
					Timestamp = mouseEventArgsList[1].Timestamp, 
					Speed = distance / timeDifference,
					XDistance = mouseEventArgsList[1].Point.X - mouseEventArgsList[0].Point.X,
					YDistance = mouseEventArgsList[1].Point.Y - mouseEventArgsList[0].Point.Y
				};
			}
			else
			{
				return null;
			}
		}

		private MouseEventInfo CreateMouseEventInfo(EventPattern<MouseEventArgs> pattern)
		{
			return new MouseEventInfo { Timestamp = pattern.EventArgs.Timestamp, Point = GetPoint(pattern) };
		}

		private string GetDebugInfoForMouseEventArgs(MouseEventInfo mouseEventInfo)
		{
			return string.Format("X: {0}, Y: {1}, Timestamp: {2}", mouseEventInfo.Point.X, mouseEventInfo.Point.Y, mouseEventInfo.Timestamp);
		}

		private double CalculateDistance(Point point1, Point point2)
		{
			return Math.Sqrt((Math.Pow(point1.X - point2.X, 2) + Math.Pow(point1.Y - point2.Y, 2)));
		}

		private Point GetPoint(EventPattern<MouseEventArgs> mouseEventArgs)
		{
			var eventArgs = mouseEventArgs.EventArgs;

			Debug.Print("GetPoint Source: {0}", eventArgs.Source.GetType().Name);

			return _mousePositionProvider.GetPosition(eventArgs);
		}

		private void AddNewPointToChartCollection(double mouseSpeedInfo)
		{
			Debug.Print("AddNewPointToChartCollection Speed: {0}", mouseSpeedInfo);

			int xPos = LineCollection.Count;
			_dispatcher.InvokeAsync(() => LineCollection.Add(new DataModel(xPos, mouseSpeedInfo)));
		}

		private class MouseEventInfo
		{
			public int Timestamp { get; set; }
			public Point Point { get; set; }
		}

		private class MouseSpeedInfo
		{
			public int Timestamp { get; set; }
			public double Speed { get; set; }
			public double XDistance { get; set; }
			public double YDistance { get; set; }
		}
	}
}