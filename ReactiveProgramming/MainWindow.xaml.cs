﻿using System.ComponentModel;
using System.Windows;

namespace ReactiveProgramming
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window, IMouseMoveEventProvider
	{
		private readonly MainWindowViewModel _mainWindowViewModel;

		public MainWindow()
		{
			InitializeComponent();

			_mainWindowViewModel = new MainWindowViewModel(this, new MousePositionProvider(this));
			DataContext = _mainWindowViewModel;
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			_mainWindowViewModel.Dispose();

			base.OnClosing(e);
		}
	}
}
