using System.Windows;
using System.Windows.Input;

namespace ReactiveProgramming
{
	public interface IMousePositionProvider
	{
		Point GetPosition(MouseEventArgs mouseEventArgs);
	}
}