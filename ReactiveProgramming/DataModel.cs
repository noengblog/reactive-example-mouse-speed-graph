﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using ReactiveProgramming.Annotations;

namespace ReactiveProgramming
{
	public class DataModel : INotifyPropertyChanged
	{
		private double _x;
		private double _y;

		public double X
		{
			get { return _x; }
			set
			{
				_x = value;
				OnPropertyChanged();
			}
		}

		public double Y
		{
			get { return _y; }
			set
			{
				_y = value;
				OnPropertyChanged();
			}
		}

		public DataModel(double x, double y)
		{
			X = x;
			Y = y;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
