﻿using System;
using System.Reactive.Linq;
using System.Windows.Input;

namespace ReactiveProgramming
{
	public class MainWindowViewModel : IDisposable
	{
		public MainWindowViewModel(IMouseMoveEventProvider mouseMoveEventProvider, IMousePositionProvider mousePositionProvider)
		{
			var mouseMove = Observable
				.FromEventPattern<MouseEventArgs>(mouseMoveEventProvider, "MouseMove");
			
			ChartViewModel = new ChartViewModel(mousePositionProvider, mouseMove);
		}

		public ChartViewModel ChartViewModel { get; private set; }
		
		
		public void Dispose()
		{
			ChartViewModel.Dispose();
		}
	}
}
